$(function() {
	
	var httpUrl = serveUrl();
	var zcQiuTokenUrl = httpUrl + '/api/qiniu/getqiniutoken'; //七牛uptoken_url接口
	var countryUrl = httpUrl+'/api/subject/getHotCountries'; //获取热门国家列表
	var provinceUrl = httpUrl+'/api/subject/getCityList';//根据父类ID获取城市列表接口
	var addSubUrl = httpUrl+'/api/subject/saveSubject';//新增专题主题接口
	var moduleIndex = 0 ;
	var mdlObj = {
		0:'banner组件',
		1:'图片广告区组件',
		2:'单品组件',
		3:'导航组件'
	};
	var typeFun = {
		'banner':zcBannerFn,
		'sgl':zcSglFn,
		'ads':zcAdsFn,
		'nav':zcNavFn
	};
	var gAddId = 0;
	var getAreaBtnId = 1;//划定热区

	var initList = function() {
	 	bindEvt();
	 	initCenterDelBtn();
	 	pageTab();
	};

	var bindEvt = function() {
		$('#module_btn').on('click',onAddModuleBtnClk); //添加一个组件
		$('#page_left_sure').on('click',onPageLeftSureClk);//左侧确认按钮点击
		$('.zj_cancle').on('click',onPageLeftCancelClk);//左侧确认按钮点击
		$('#zc_editor_wp').on('click','.add_one_banner',onAddOneBannerTap);//添加一个新轮播
		$('#zc_editor_wp').on('change','.evy_slider .slider_select',4,onSliderSelChange); //下拉框处理（旅游产品详情）
		$('#zc_editor_wp').on('change','.jump_type_sel',4,onSliderSelChange); //下拉框处理（旅游产品详情）
		$('#zc_editor_wp').on('change','.single_pdt_sel',3,onSliderSelChange); //下拉框处理（旅游产品详情）
		$('#zc_editor_wp').on('change','.jump_type_sel',onJumpTypeSelChange); //广告热区下拉框处理（旅游产品详情）
		$('#zc_editor_wp').on('change','.zc_country',onZcCountryChange); //下拉框处理（国家||省份||城市）
		$('#zc_editor_wp').on('change','.zc_province',onZcProvinceChange); //下拉框处理（国家||省份||城市）
		$('#zc_editor_wp').on('click','.slider_right',onSliderRightClk); //删除按钮点击
		$('#zc_editor_wp').on('click','.add_new_aera',onAddNewAeraClk);//添加一个热区
		$('#zc_editor_wp').on('click','.add_one_tab',onAddOneTabClk);//添加一个tab
		$('#zc_editor_wp').on('click','.add_one_pdt',onAddOnePdtClk);//添加一个pdt
		$('#zc_editor_wp').on('click','.add_one_anchor',onAddOneAnchorClk);//添加一个pdt
		$('#zc_editor_wp').on('click','.del_onepdt_btn',onDelOnePdtBtnClk);//删除一个pdt
		$('#zc_scan_wp').on('click','.moveup',onMoveUpClk);//上移动一个元素
		$('#zc_scan_wp').on('click','.movedown',onMoveDownClk);//下移动一个元素
		$('#zc_scan_wp').on('click','.thiseditor',onThiseDitorClk);//编辑一个元素
		$('#zc_scan_wp').on('click','.thisdel',onThisDelClk);//删除一个元素
		$('#zc_editor_wp').on('click','.del_this',onDelThisBtnClk);//删除一个热区
		$('#zc_editor_wp').on('click','.hot_set label',onHotLabelClk);//选择热区是全图||无||分区
		$('#zc_editor_wp').on('click','.get_area',onGetAreaClk);//点击可划定热区
		/*以下是页面保存和修改取消按钮！*/
		$('.page_sure_btn').on('click',onPageSureBtnClk); //页面信息保存
		$('.page_cancel_btn').on('click',onPageCancelBtnClk);//修改取消
	};

	



	function zcBannerFn($obj) {
		var banObj = {type:'banner'};
		var $currSliders = $obj.find('.evy_slider');
		
		banObj.anchorLocaltion = $obj.find('.ban_anchor').val();
		banObj.sliders = [];
		$.each($currSliders, function(i, el) {
			var obj = {};
			obj.linkType = $(el).find('.jump_type_sel').val();//跳转类型
			obj.title = $(el).find('.ban_title').val(); //标题
			obj.jumpLink = $(el).find('.jump_link').val();
			obj.dataCheck = $(el).find('.data_check').val();
			obj.pic = $(el).find('.zcQiniuImg').attr('src');
			if(obj.linkType==4) {
				obj.travelSecondType = $(el).find('.travel_types').val();//如果是4则多出travelSecondType字段
			}else if(obj.linkType==5||obj.linkType==6||obj.linkType==7) {
				obj.cityCode=$(el).find('.zc_area').val();
			}
			banObj.sliders.push(obj);
		});
		return banObj;
	};
	function zcNavFn($obj) {
		var navObj = {type:'nav'};
		var $navNodes = $obj.find('.evy_nav');
		navObj.nodes = [];
		navObj.hang = $obj.find('input[name=xuanfu]:checked').attr('data-val');//是否悬浮
		// console.log(navObj.hang);
		navObj.navType = $obj.find('input[name=nav_type]:checked').attr('data-val');//横向||竖向
		navObj.pic = $obj.find('.zcQiniuImg').attr('src');
		$.each($navNodes, function(i, el) {
			 var obj = {};
			 obj.name = $(el).find('.anchor_name').val();
			 obj.anchorLocaltion = $(el).find('.anchor_loc').val();
			 navObj.nodes.push(obj);
		});
		return navObj;
	};

	function zcSglFn($obj) {
		var sglObj = {type:'sgl'};
		var $tabs = $obj.find('.evy_single_pdt');
		sglObj.tabs = [];
		sglObj.sort = $obj.find('input[name=pailietypeo]:checked').attr('data-val');//宽||窄
		sglObj.anchorLocaltion = $obj.find('.sgl_anchor').val();//锚点位置
		$.each($tabs, function(i, el) {
			 var obj={};
			 var $pdt = $(el).find('.evy_filtered');
			 obj.tabLabel=$(el).find('.tab_label_name').val();
			 obj.dataCheck = $(el).find('.data_check').val();
			 obj.product = zcSglChildFn($pdt); //[]数组
			 sglObj.tabs.push(obj);
		});

		return sglObj;
	};

	function zcAdsFn($obj) { //广告热区组件
		var adsObj = {type:'ads'};
		var $hotAreas = $obj.find('.hot_type_wp')||null;
		adsObj.imgWidth = $('.hot_img_wp').width()+'px'||'320px';//用于比例运算@zc20170817
		adsObj.pic = $obj.find('.zcQiniuImg').attr('src');
		adsObj.hotType = $obj.find('.hot_set .hot_lab.on').attr('data-val');//热区类型：全图allPic||无noHot||分区divide
		adsObj.anchorLocaltion = $obj.find('.ads_anchor').val();//锚点位置
		adsObj.hotAreas = [];
		if(adsObj.hotType!='noHot') {
			$.each($hotAreas, function(i, el) {
				var obj={};
				var id = -1;//热区id
				var $target = null;
				obj.type=$(el).find('.jump_type_sel').val();
				obj.linkType = $(el).find('.jump_link').val();//跳转链接
				obj.dataCheck = $(el).find('.data_check').val();//数据检测
				if(adsObj.hotType==='divide') { //如果划定了热区，则增加一个字段
					id = $(el).find('.get_area').attr('curr_id');//需要验证TODO
					$target = $obj.find('.img_masker .zc_hot_div[curr_id='+id+']');
					obj.areaTop = $target.css('top');
					obj.areaLeat = $target.css('left');
					obj.areaWidth = $target.css('width');
					obj.areaHeight = $target.css('height');
				}
				if(obj.type==4) { //旅游产品详情，需要子分类
					obj.travel_type=$(el).find('.travel_types').val();
				}else if(obj.type==5||obj.type==6||obj.type==7) {
					obj.cityCode = $(el).find('.zc_area').val();
				}else if(obj.type==1) {
					obj.giftSuccess = $(el).find('.success_input').val();
					obj.giftFail = $(el).find('.fail_input').val();
				}
				adsObj.hotAreas.push(obj);
			});
		}
		return adsObj;
	}

	function zcSglChildFn($obj) { //单品子函数
		var arr = [];
		$.each($obj, function(i, el) {
			 var obj = {};
			 obj.type = $(el).find('.single_pdt_sel').val();
			 obj.productId = $(el).find('.pdtid').val();
			 if(obj.type==3) {
			 	obj.travel_type = $(el).find('.travel_types').val();
			 }
			 arr.push(obj);
		});
		return arr;
	};


	var onPageSureBtnClk = function() {//页面信息保存
		var subject = {};
		var dataList = [];
		var $typeDiv = $('.zj_set_wp>div');
		var ajaxData = {};
		var ajaxDataStr = '';
		subject.subName = $('#sub_name').val();
		subject.diyLink = $('#diy_link').val();
		subject.subShare = $('#share_sub').val();
		subject.wordColor = $('#word_color').val();
		subject.bgColor = $('#bg_color').val();
		subject.goTop = $('input[name=asdfg]:checked').attr('gototop')//0:是，1:否
		$.each($typeDiv, function(i, el) {
			var type = $(el).attr('data-type');
			var obj = typeFun[type]($(el));
			dataList.push(obj)
			console.log(obj);
		});
		console.log(dataList);
		ajaxData={
			subject:subject,
			dataList:dataList
		};
		ajaxDataStr=JSON.stringify(ajaxData);
		console.log(ajaxDataStr);
		$.ajax({//JSON.stringify(dataList)
			type:'POST',
			contentType:'application/json;charset=UTF-8',
			url:addSubUrl,
			data:ajaxDataStr,
			success:function(res) {
				console.log(res);
			},
			error:function(xhr,qwe) {
				console.log('ajax error');
				console.log(xhr);
				console.log(qwe);
			}

		})
	};

	var onPageCancelBtnClk = function() {//修改取消

	};

	var onZcProvinceChange = function() {
		var val = $(this).val();
		var $dom = $(this).parents('.alllist').find('.zc_area');
		renderProinceByParCode(val,$dom,'请选择市');
	};

	var onZcCountryChange = function() {
		var val = $(this).val();
		var $dom = $(this).parents('.alllist').find('.zc_province');
		$(this).parents('.alllist').find('.zc_area').html('请选择市');
		renderProinceByParCode(val,$dom,'请选择省');
	};

	var onGetAreaClk = function() {
		var currId = $(this).attr('curr_id');
		if(!currId) {
			$(this).attr('curr_id',getAreaBtnId);
			drawHotArea( $('.zj_set_wp .img_masker'),isOverlap,getAreaBtnId );
			getAreaBtnId++;
		}else {
			// console.log($('.img_masker>div'))
			if(  testIn( currId,$('.img_masker>div') )  ) {
				alert('请先删掉已经划定的热区');
				return;
			}
			drawHotArea( $('.zj_set_wp .img_masker'),isOverlap,currId );
		}
	};

	var onHotLabelClk= function() {
		var index = $(this).index();
		if(index==3||index==1) {
			$(this).siblings('.add_new_aera').addClass('show_out');
			$(this).parent().siblings('.hot_type_wp').addClass('show_out');
		}else {
			$(this).siblings('.add_new_aera').removeClass('show_out');
			$(this).parent().siblings('.hot_type_wp').removeClass('show_out');
		}
		$(this).addClass('on').siblings('label').removeClass('on');
	};

	var onDelThisBtnClk = function() {

		$(this).parents('.zc_hot_div').remove();
	};

	var onThiseDitorClk = function() { //编辑一个组件
	 	var $par = $(this).parents('.mdl_name');
	 	var id = $par.attr('data_id');
	 	$('.zj_set_wp>div[data-id='+id+']').addClass('show_out curr_dom editing');//添加正在编辑标志editing
	 	$('.editing_curr_mdl').html($('.zj_set_wp>div[data-id='+id+']').clone(true, true));
	 	$('.editing_curr_mdl').find('.slider_select').val($('.zj_set_wp>div[data-id='+id+']').find('.slider_select').val());
	 	$('.editing_curr_mdl').find('.travel_types').val($('.zj_set_wp>div[data-id='+id+']').find('.travel_types').val());
	 	// $('.zj_set_wp>div[data-id='+id+']').clone(true, true).appendTo('.editing_curr_mdl');

	};

	var onThisDelClk = function() {//删除一个组件
	 	var $par = $(this).parents('.mdl_name');
	 	var id =  $par.attr('data_id');
	 	$par.remove();
	 	$('.zj_set_wp>div[data-id='+id+']').remove();
	};

	var onMoveDownClk = function() {//下移动一个组件
	 	var $upDiv = $(this).parents('.mdl_name').next();
	 	var $currDiv = $(this).parents('.mdl_name');
	 	var $leftDiv1 = $('.zj_set_wp .page_zj[data-id='+$upDiv.attr('data_id')+']');
	 	var $leftDiv2 = $('.zj_set_wp .page_zj[data-id='+$currDiv.attr('data_id')+']');
	 	var len = $upDiv.size();
	 	if (!len) {return;}
	 	swap($upDiv,$currDiv);
	 	swap($leftDiv1,$leftDiv2);
	};

	var onMoveUpClk = function() {//上移动一个组件
	 	var $upDiv = $(this).parents('.mdl_name').prev();
	 	var $currDiv = $(this).parents('.mdl_name');
	 	var $leftDiv1 = $('.zj_set_wp .page_zj[data-id='+$upDiv.attr('data_id')+']');
	 	var $leftDiv2 = $('.zj_set_wp .page_zj[data-id='+$currDiv.attr('data_id')+']');
	 	var len = $upDiv.size();
	 	if (!len) {return;}
	 	swap($upDiv,$currDiv);
	 	swap($leftDiv1,$leftDiv2);
	};

	var onAddOneAnchorClk = function() {
	 	$('.evy_zj_wp .evy_nav').eq(0).clone().appendTo('.zj_set_wp .anchor_wp');
	};

	var onJumpTypeSelChange = function() {
	 	var currVal = $(this).val();
	 	var $counSel = null;
	 	var $prvnSel = null;
	 	var $areaSel = null;
	 	var $alllist = $(this).siblings('.alllist');
	 	// console.log($(this).siblings('.gift_word'));
	 	if(currVal==1) {
	 		$(this).siblings('.gift_word').addClass('show_out');
	 	}else {
	 		$(this).siblings('.gift_word').removeClass('show_out');
	 		if(currVal==5||currVal==6||currVal==7) {
	 			$alllist.addClass('show_out');
	 			$counSel = $alllist.find('.zc_country');
	 			renderCountry($counSel);
	 		}else {
	 			$(this).siblings('.alllist').removeClass('show_out');
	 		}
	 	}
	};

	var onDelOnePdtBtnClk = function() { //删除一个pdt
	 	var len = $(this).parents('.evy_filtered').siblings('.evy_filtered').size();
	 	len>0 && $(this).parents('.evy_filtered').remove();
	};

	var onAddOnePdtClk =function() {
	 	$('.evy_zj_wp .evy_filtered').clone().appendTo('.zj_set_wp .pdt_filter_wp');
	};

	var onAddOneTabClk = function() {
	 	$('.evy_zj_wp .evy_single_pdt').clone().appendTo('.zj_set_wp .single_zj');
	};


	var onAddNewAeraClk = function() {
	 	$('.evy_zj_wp .hot_type_wp').clone().addClass('show_out').appendTo('.zj_set_wp .img_zj');
	};

	var onSliderRightClk= function() { //删除一个模块
		var id = -1;
	 	var len = 1+Number($(this).parents('.left_right_wp').siblings('.left_right_wp').size()) ;
	 	if(len>1) {
	 		if($(this).parents('.page_zj').hasClass('img_zj')) {
	 			id = $(this).siblings('.slider_left').find('.get_area').attr('curr_id');
	 			$('.img_masker>div[curr_id='+id+']').remove();
	 		}
	 		$(this).parents('.left_right_wp').remove();
	 	}else {
	 		console.log('后期再处理TODO');
	 	}
	 	
	};

	var onSliderSelChange = function(ev) {
	 	var currVal = $(this).val();
	 	if(currVal==ev.data) {
	 		$(this).siblings('.travel_types').addClass('show_out');
	 	}else {
	 		$(this).siblings('.travel_types').removeClass('show_out');
	 	}
	};

	var onAddOneBannerTap = function() { //添加一个轮播位
	 	var btn = null,$picDom = null;
	 	$('.evy_zj_wp .evy_slider').clone().appendTo('.zj_set_wp .slider_wp');
	 	btn = $('.zj_set_wp .slider_wp .evy_slider:last-child .qiniuBtn').get(0);
	 	$picDom = $('.zj_set_wp .slider_wp .evy_slider:last-child .zcQiniuImg');
	 	zcCreartQiniu(btn,$picDom);
	};

	var pageTab = function() {
	 	var $tabHedaers = $('.header>div');
	 	var $tabBodys = $('.tab_body>div');
	 	$tabHedaers.on('click',function() {
	 		var index = $(this).index();
	 		$(this).addClass('on').siblings('.on').removeClass('on');
	 		$tabBodys.css('display','none').eq(index).css('display','block');
	 		if(index==1) {
	 			centerSliderRight($('.zj_set_wp>div.show_out').find('.slider_right'));
	 		}
	 	})
	};

	var onPageLeftSureClk = function() { //左侧确认按钮点击
	 	var index = $('.zj_set_wp .curr_dom').index();
	 	var isEditing = $('.zj_set_wp .curr_dom').hasClass('editing');
	 	if(index<0){return;}
	 	if(!isEditing) {
	 		$('.zj_set_wp .curr_dom').attr('data-id',gAddId).removeClass('curr_dom show_out');
		 	$('.mdl_name_wp .mdl_name').find('.mdl_word').html(mdlObj[moduleIndex]).end().attr('data_id',gAddId).clone().appendTo('#scan_area_cnt');
		 	gAddId++;
		 }else {
		 	$('.zj_set_wp .curr_dom').removeClass('editing curr_dom show_out');
		 }
	};

	var onPageLeftCancelClk = function() { //编辑取消
	 	var id = $('.editing_curr_mdl>div').attr('data-id');
	 	$('.zj_set_wp>div[data-id='+id+']').replaceWith($('.editing_curr_mdl>div'));
	};


	var onAddModuleBtnClk = function() { 
	 	var val = $('#selest_zj option:checked').val();
	 	moduleIndex = val;
	 	var btn = null,$picDom = null;
	 	if(val==10) {return;}
	 	// $('#zc_editor_wp .may_show_out').removeClass('show_out').eq(val).addClass('show_out');
	 	$('.zj_set_wp .curr_dom').remove();//先删掉未确认的组件
	 	$('.evy_zj_wp .may_show_out').eq(val).clone().addClass('show_out curr_dom').appendTo($('.zj_set_wp'));//將克隆的組件扔进目标元素并添加curr_dom标记，当点击左侧确认时，去掉该标记，将该组件标记到右侧
	 	centerSliderRight($('.zj_set_wp>div.show_out').find('.slider_right'));
	 	if(val!=10&&val!=2) {
	 		btn = $('.zj_set_wp .show_out.curr_dom').find('.qiniuBtn').get(0);
	 		$picDom = $('.zj_set_wp .show_out.curr_dom').find('.zcQiniuImg');
	 		zcCreartQiniu(btn,$picDom);
	 	}
	};

	var initCenterDelBtn = function() {
	 	var $delBtns = $('.slider_right');
	 	centerSliderRight($delBtns);
	};

	function renderCountry($selDom) {
		$.ajax({
			type:'POST',
			contentType:"application/json;charset=UTF-8",
			url:countryUrl,
			success:function(res) {
				console.log(res);
				var data=[];
				var str = '<option value="0">请选择国家</option>';
				if(res.code==0) {
					data = res.data;
					$.each(data, function(index, val) {
						str+='<option value='+val.cityCode+'>'+val.countryName+'</option>'
					});
					$selDom.html(str);
				}else {
					console.log('返回码'+res.code);
				}
			},
			error:function() {
				console.log('ajax error');
			}
		})
	}

	function renderProinceByParCode(code,selDom,firstItem) {
		// console.log(selDom);
		var ajax_data_str = JSON.stringify({parentCode:code});
		$.ajax({
			type:'POST',
			url:provinceUrl,
			contentType:"application/json;charset=UTF-8",
			data:ajax_data_str,
			success:function(res) {
				console.log(res);
				var data = [];
				var str = '<option value="0">'+firstItem+'</option>';
				if(res.code==0) {
					data = res.data;
					$.each(data, function(index, val) {
							str+='<option value='+val.cityCode+'>'+val.cityName+'</option>'	
					});	
					selDom.html(str);
				}else {
					console.log('返回码'+res.code);
				}
			},
			error:function(xhr) {
				console.log('ajax error');
				console.log(xhr);
			}
		})
	}

	function testIn(id,$obj) {
		var r = false;
		$.each($obj, function(index, val) {
			if($(val).attr('curr_id')==id) {
				r=true;
				return false;
			}
		});
		return r;
	}

	function isOverlap($curr,$all) {
		$.each($all, function(index, val) {
			if($curr.get(0)!=val) {
			 	if(testDivOverlap($curr,$(val))) {
			 		$curr.remove();
			 		return false;
			 	}
			}
		});
	}

	function testDivOverlap($div1,$div2) {
		var r = false;
		var ftX1 = $div1.position().left,	
			ftX2 = ftX1+$div1.outerWidth(),
			ftY1 = $div1.position().top,
			ftY2 = ftY1+$div1.outerHeight(),
			sdX1 = $div2.position().left,	
			sdX2 = sdX1+$div2.outerWidth(),
			sdY1 = $div2.position().top,
			sdY2 = sdY1+$div2.outerHeight();
		if(   ( sdX1<=ftX2&&sdX1>=ftX1||sdX2>=ftX1&&sdX2<=ftX2 ) && ( sdY1>=ftY1&&sdY1<=ftY2||sdY2>=ftY1&&sdY2<=ftY2 )   ) {
			r = true;//重合
		} else {
			r = false;//没有重合
		}
		return r;
	}

	function drawHotArea($dom,fn,curr_id) {
	 	var wId = "w";
		var index = 0;
		var startX = 0, startY = 0;
		var flag = false;
		var retcLeft = "0px", retcTop = "0px", retcHeight = "0px", retcWidth = "0px";
		var left = '0px',top='0px',width='0px',height='0px';
		var currId = '';
		$dom.one('mousedown',function(ev) {
			// console.log(ev);
			index++;
			currId = wId+index;
			flag=true;
			startX = ev.offsetX;
			startY = ev.offsetY;
			$('<div class=div id='+currId+'></div>').css({'left':startX+'px','top':startY+'px'}).appendTo($dom);
		});
		$dom.on('mousemove',function(ev) {
			if(flag) {
				// console.log(ev);
				left   = (ev.offsetX-startX>0?startX:ev.offsetX)+'px';
				top    = (ev.offsetY-startY>0?startY:ev.offsetY)+'px';
				width  = Math.abs(ev.offsetX-startX)+'px';
				height = Math.abs(ev.offsetY-startY)+'px';
				$('#'+wId+index).css({
					'left'   : left,
					'top'    : top,
					'width'  : width,
					'height' : height
				})
			}
		});
		$(document).on('mouseup',function(ev) {
			var $this = $('<div class=zc_hot_div></div>');
			$('#'+wId+index).remove();
			$('.div').remove();
			if(parseInt(width)>30&&parseInt(height)>30) {
				$this.css({
					'left'   : left,
					'top'    : top,
					'width'  : width,
					'height' : height
				}).attr('curr_id',curr_id).appendTo($dom).append('<div class="del_this">x</div>');

			}
			retcLeft = "0px"; retcTop = "0px"; retcHeight = "0px"; retcWidth = "0px";
			left = '0px';top='0px';width='0px';height='0px';
			
			if(flag && fn && typeof fn ==='function') {
				fn($this,$('.zc_hot_div'));
			}
			flag = false;
		});
		
	}

	function zcCreartQiniu(zcBtn,$img) { //创建七牛实例，传递上传按钮id,$img,(zcBtn:id||dom)
	 	
	 	new QiniuJsSDK().uploader({
	 		runtimes: 'html5,flash,html4',      // 上传模式，依次退化
		    browse_button: zcBtn,         // 上传选择的点选按钮，必需
		    uptoken_url:zcQiuTokenUrl,
		    get_new_uptoken: false,             // 设置上传文件的时候是否每次都重新获取新的uptoken
		    auto_start: true,                   // 选择文件后自动上传，若关闭需要自己绑定事件触发上传
		    domain:'http://img.tgljweb.com/',
		    init:{
		    	FileUploaded: function(up, file, info) {
		    		var httphead = up.getOption('domain');
		    		var key = JSON.parse(info.response).key;
		    		$img.attr('src',(httphead+key));
		    		//热区图片上传成功后，将所有已划定热区干掉！！
		    		if($(zcBtn).hasClass('img_upload_btn')) {
		    			$('.img_masker').html('');
		    			$.each($('.zj_set_wp .img_zj .hot_type_wp'), function(index, val) {
		    				console.log(index);
		    				console.log(val);
		    				if(index!=0) {
		    				 	$(val).remove();
		    				}
		    			});
		    		}
	          	},
	          	Error: function(up, err, errTip) {
		        	console.log(err);
		        	console.log(errTip);
		        }

		    }
	 	})
	}

	function swap(a,b) { //交换两个div的位置
		 var a1 = $("<div id='a1'></div>").insertBefore(a);
		 var b1 = $("<div id='b1'></div>").insertBefore(b);
 		 a.insertAfter(b1);
 		 b.insertAfter(a1);
 		 a1.remove();
 		 b1.remove();
		 a1 = b1 = null;
    }

	function filterClass(classes) {
	 	var arr = classes.split(' ');
	 	var result =arr.filter(function(item) {
	 		return item!=''&&item!=' '&&item!='ctrl_p'&&item!='may_show_out'&&item!='show_out'&&item!='page_zj';
	 	});
	 	return result[0];
	}

	function centerSliderRight(targets) {
	 	$.each(targets, function(index, val) {
	 		var height = $(val).height()+'px';
	 		$(val).css('lineHeight',height);
	 	});
	}


	initList();
}) 

/*
	专题列表页面所需

	1、保存页面数据接口

	2、查询单条数据接口（数据列表页面点击修改按钮时需要）

	3、城市下拉接口（国家/省/城市）

	4、数据列表接口

	5、删除单条数据接口

	6、启用单条数据接口



*/