module.exports = {
	entry:{
		pageEditor:__dirname+'/entryjs/pageEditor.js',
	},
	output:{
		path:__dirname+'/outputjs',
		filename:'[name].js'
	},
	module:{
		loaders:[
			{
				test:/\.css$/,
				loader:'style-loader!css-loader'
			}
		]
	},
	
}